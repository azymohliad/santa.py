#!/usr/bin/env python

import os
import json
import random
import argparse
import smtplib
import ssl
import email.mime.text
from typing import Set, Dict


def load_participants(filename: str) -> Dict[str, dict]:
    '''Reads participants info from input JSON file
    '''
    # Read JSON file
    with open(filename, 'r') as fp:
        participants = json.load(fp)
    # Validate parsed dictionary
    required_fields = {'email'}
    known_fields = {'email', 'exceptions'}
    for name, data in participants.items():
        # Check required fields
        missing_fields = required_fields - data.keys()
        if missing_fields:
            raise ValueError(f'Missing info for "{name}": ' + ', '.join(missing_fields))
        # Check unknown fields
        unknown_fields = data.keys() - known_fields
        if unknown_fields:
            print(f'Unexpected info for "{name}": ' + ', '.join(unknown_fields))
        # Check exceptions
        exceptions = data.setdefault('exceptions', [])
        unknown_names = exceptions - participants.keys()
        if unknown_names:
            raise ValueError(f'The following exceptions for "{name}" are not found: ' + ', '.join(unknown_names))
    return participants

def assign_receivers(participants) -> Dict[str, str]:
    '''Returns randomized mapping of participants to their gift receivers names
    '''
    receivers = {}
    # Sort participants to process the ones with less prior choices first,
    # to decrease the probability of randomization failure
    sort_key = lambda item: len(item[1]['exceptions'])
    santa_pool = [k for k,v in sorted(participants.items(), key=sort_key, reverse=True)]
    receiver_pool = set(participants.keys())
    for santa in santa_pool:
        exceptions = set(participants[santa]['exceptions'])
        pool = receiver_pool - exceptions - {santa}
        if len(pool) == 0:
            # TODO: Fix this properly, without making the algorithm overcomplicated
            raise ValueError('Oops, randomization failed, no suitable gift receivers left. Try again!')
        receiver = random.choice(list(pool))
        receivers[santa] = receiver
        receiver_pool.remove(receiver)
    return receivers

def send_emails(participants, receivers, login, password=None, server='smtp.gmail.com', port=465, debug=False):
    '''Notifies participants about their gift receivers over email
    '''
    sender = f'Таємний Санта<{login}>'
    subject = 'Розподіл Таємних Сант!'
    message = 'Хо-хо-хо!!!\n\nОтримувач твого подарунку: {receiver}!\n\nЗ наступаючими святами!'
    if password is None:
        password = input('Email password (not hidden):')

    with smtplib.SMTP_SSL(server, port, context=ssl.create_default_context()) as server:
        server.login(login, password)
        for santa, data in participants.items():
            recipient = data['email']
            if debug:
                # Send back to the sender email for debug purposess
                suffix = recipient.split('@')[0]
                recipient = login.replace('@', f'+{suffix}@')
            receiver = receivers[santa]
            mime = email.mime.text.MIMEText(message.format(receiver=receiver))
            mime['From'] = sender
            mime['To'] = recipient
            mime['Subject'] = subject
            server.sendmail(sender, recipient, mime.as_string())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Secret Santa randomizer')
    parser.add_argument('input_file', help='Input filename (JSON)')
    parser.add_argument('-o', '--output-file', help='Output filename')
    parser.add_argument('-v', '--verbose', action='store_true', help='Print results to stdout')
    parser.add_argument('-m', '--email', help='Email address to send the results from (omit to not send emails)')
    parser.add_argument('--email-password', help='Email password (will be asked if omitted but needed)')
    parser.add_argument('--smtp-server', default='smtp.gmail.com', help='SMTP server')
    parser.add_argument('--smtp-port', default=465, type=int, help='SMTP port')
    parser.add_argument('--debug-email', action='store_true', help='Send emails back to sender instead of participants for debug purposes')
    args = parser.parse_args()
    if not (args.output_file or args.email or args.verbose):
        print('Select at least one way to output results: --output-file, --verbose, or --email')
        exit(1)

    try:
        # Load input file
        participants = load_participants(args.input_file)
        # Randomize
        receivers = assign_receivers(participants)
    except ValueError as ex:
        print(ex)
        exit(1)

    # Save results to output file
    results = ''.join([f'{s} -> {r}\n' for s, r in receivers.items()])
    if args.output_file:
        with open(args.output_file, 'w') as fp:
            fp.write(results)

    # Print results
    if args.verbose:
        print(f'Result:\n{results}')

    # Email results
    if args.email:
        print('Sending emails...')
        send_emails(participants, receivers, args.email, args.email_password, args.smtp_server, args.smtp_port, args.debug_email)
